;;; ix.io.el --- Utility functions for interacting with `ix.io' -*- lexical-binding: t; -*-

;; Copyright 2020–2022  Tony Zorman
;; Homepage: https://gitlab.com/slotThe/ix.io
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides utility functions to easily paste snippets to `ix.io'.
;; Example `use-package' configuration:
;;
;;   (use-package ix.io
;;     :load-path "~/.config/emacs/elisp/"
;;     :bind (("C-x <f12>" . ix.io-paste-region)
;;            ("C-x <f11>" . ix.io-paste-file  )))
;;

;;; Code:

(defgroup ix.io nil
  "The `ix.io' group."
  :group 'applications)

(defcustom ix.io-output-buffer "temp-ix.io-output-buffer"
  "Temporary buffer to capture the request output.
Will be killed after copying the URL over to the primary
selection."
  :type 'string
  :group 'ix.io)

(defcustom ix.io-error-buffer "temp-ix.io-error-buffer"
  "Temporary buffer to capture any error messages.
Will be killed after the URL has been copied over from
`ix.io-output-buffer' to the primary selection."
  :type 'string
  :group 'ix.io)

(defun ix.io--upload-file-get-link (file)
  "Upload the given FILE to `ix.io'.

After we get a response from the site, the generated URL is
copied into the primary selection and the created buffers are
killed.  A file with the pasted content will be created in the
`/tmp' directory."
  (shell-command (concat "curl -F " " 'f:1=@" file "' ix.io")
                 ix.io-output-buffer    ; this will only contain the link
                 ix.io-error-buffer)    ; curl nonsense
  ;; Get URL and copy it to primary selection.
  (with-current-buffer (get-buffer ix.io-output-buffer)
    (let ((select-enable-primary t))
      (kill-region (line-beginning-position) (line-end-position))))
  ;; Clean up.
  (mapc #'kill-buffer `(,ix.io-output-buffer ,ix.io-error-buffer)))

;;;###autoload
(defun ix.io-paste-file (&optional file)
  "Paste a file to `ix.io'.
Copies the resulting link to the primary selection.

Unless the optional argument FILE is given, choose file
interactively.

See `ix.io--upload-file-get-link' for additional information."
  (interactive)
  (let ((file (or file
                  (expand-file-name (read-file-name "File to paste: ")))))
    (ix.io--upload-file-get-link file)))

;;;###autoload
(defun ix.io-paste-region (beg end)
  "Paste text between point and mark to `https://ix.io'.
Pastes the stretch of text between BEG and END to the above
pasting site and copies the resulting link to the primary
clipboard.

Note that you will be asked for confirmation before anything is
done, in order to avoid accidental pastes.

See `ix.io--upload-file-get-link' for additional information."
  (interactive "r")
  (unless (use-region-p)
    (user-error "The mark is not set now, so there is no region"))
  (let* ((str (filter-buffer-substring beg end))
         (tmp-file (format "/tmp/ix.io-file-%s" (length str))))
    ;; Accidental presses would be a bother.
    (when (and str (y-or-n-p "Really upload the current selection? "))
      (write-region str nil tmp-file)
      (ix.io--upload-file-get-link tmp-file)
      ;; Clean up.
      (pop-mark))))

(provide 'ix.io)
;;; ix.io.el ends here
