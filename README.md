# ix.io

Utility functions to easily paste snippets to `ix.io'.  Example
`use-package' configuration:

``` emacs-lisp
(use-package ix.io
  :load-path "~/.config/emacs/elisp/"
  :bind (("C-x <f12>" . ix.io-paste-region)
         ("C-x <f11>" . ix.io-paste-file  )))
```
